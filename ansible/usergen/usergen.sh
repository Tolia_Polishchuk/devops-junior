#!/bin/bash

user="$(grep '^tolia' /etc/passwd | cut -b 1-5)"
node="$(hostname | cut -b 1-6)"

if [ "$user" != "tolia" ]; then

  sudo useradd -m -G sudo -s /bin/bash tolia
  sudo echo "tolia ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
  sudo mkdir /home/tolia/.ssh
  sudo touch /home/tolia/.ssh/authorized_keys
  if [ "$node" == "master" ]; then
    sudo cp -v /vagrant/Keys/Key /home/tolia/.ssh
  else
    sudo cp -v /vagrant/Keys/Key.pub /home/tolia/.ssh/authorized_keys
    mkdir /home/tolia/docker    
  fi
  chown tolia:tolia /home/tolia/.ssh/authorized_keys
  sudo systemctl restart sshd

fi


