#!/bin/bash

echo "Stoping containers..."
echo ""
docker stop $(docker ps -q)
echo "Deleting images..."
echo ""
docker rmi $(docker images -q) -f
echo "Deleting containers..."
docker rm $(docker ps -aq)


