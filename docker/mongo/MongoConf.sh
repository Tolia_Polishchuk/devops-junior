#!/bin/bash
#mongo --eval 'db.runCommand({ connectionStatus: 1 })'

echo "Creating collections and users..."
# Admin & root users

mongo <<EOF
use hellodb
db.nodejs.insertOne(
  {
    data: "Hello,world!"
  }
)
db.createUser({  
 user:"node",
 pwd:"8442",
 roles:[  
  {  
     role:"dbOwner",
     db:"hellodb"
  }
 ]
})
EOF
